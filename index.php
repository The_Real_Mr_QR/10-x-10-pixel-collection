<?php

$x = 10;
$y = 10;

$new_image = imagecreatetruecolor($x, $y);
$bg_color = imagecolorallocate($new_image,  255, 255, 255);
$pixel_color = imagecolorallocate($new_image,  0, 0, 0);
imagefill($new_image, 0, 0, $bg_color);

for ($xG = 0; $xG < $x; $xG++) {
    for ($yG = 0; $yG < $y; $yG++) {
        $xStr = strval($xG+1);
        $yStr = strval($yG+1);
        imagesetpixel($new_image, $xG, $yG, $pixel_color);
        header('Content-Type: image/png');
        imagepng($new_image, 'output/x'.$xStr.'_y'.$yStr.'.png');
        imagedestroy($new_image);
        imagesetpixel($new_image, $xG, $yG, $bg_color);
    }
}